<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://linkedin.com/in/minhtuan2086
 * @since      1.0.0
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/public
 * @author     TUAN NGUYEN minh <minhtuan2086@gmail.com>
 */
class Yup_Airbnb_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->yup_options = get_option($this->plugin_name);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Yup_Airbnb_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Yup_Airbnb_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name . '-bootstrap-select', '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/yup-airbnb-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Yup_Airbnb_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Yup_Airbnb_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$gmaps_url = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCwbt1Y6Mzwn-f0Jn3xxXDHgsGqpfRxSiU&sensor=false';

		wp_enqueue_script( $this->plugin_name .'-bootstrap-select', '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name .'-bootstrap-datepicker', plugin_dir_url( __FILE__ ) . 'js/bootstrap-datepicker.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name .'-handlebars', '//cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js', array( 'jquery' ), $this->version, true );

		wp_register_script('googlemaps', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCwbt1Y6Mzwn-f0Jn3xxXDHgsGqpfRxSiU', false, '3');
		wp_enqueue_script('googlemaps');

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/yup-airbnb-public.js', array( 'jquery' ), $this->version, true );

		wp_localize_script( $this->plugin_name,'ret_init_listing_ajax', array('ajax_url' => admin_url('admin-ajax.php') ));
		wp_localize_script( $this->plugin_name,'ret_search_listing_ajax', array('ajax_url' => admin_url('admin-ajax.php')));

	}

	/**
	 * Get INTI list of Listing House
	 *
	 * @since    1.0.0
	 */

	 public function yupinits(){
	 	# code...

			$cid = $this->yup_options['yupclientid'];
			$uid = $this->yup_options['yupuid'];
			$url = 'https://api.airbnb.com/v2/listings';

			//check if these options not set
			if( empty( $cid) || empty($uid)){
					$uid = '57297136';
					$cid = '3092nxybyb0otqw18e8nh5nty';
			}

			//set data
			$data = array(
					'client_id' => $cid,
					'locale' => 'en-US',
					'currency' => 'USD',
					'_format' => 'v1_legacy_long',
					'_limit' => '50',
					'_offset'	=> '0',
					'has_availability' =>	'false',
					'user_id' => 	$uid
			);

			 $params = '';

			 foreach($data as $key=>$value)
									 $params .= $key.'='.$value.'&';

				$params = trim($params, '&');

			//  Initiate curl
								$ch = curl_init();
								// Disable SSL verification
								curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
								// Will return the response, if false it print the response
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

								// Set the url
								 curl_setopt($ch, CURLOPT_URL, $url.'?'.$params );
								// Execute
								$result=curl_exec($ch);

								//Get the resulting HTTP status code from the cURL handle.
								$http_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

								// Closing
								curl_close($ch);

								$ret  = json_decode($result, true);




								echo "<script type=\"text/javascript\">/* <![CDATA[ */

										var yupMeta = ". json_encode($ret['metadata']) ."  ;
										var yupInitListing = ". json_encode($ret['listings']) ."  ;

									/* ]]> */</script>";

	 }



	 /**
 	 * Check Available of Listing House
 	 *
 	 * @since    1.0.0
 	 */


		function CheckHouseAvailable($listing,$checkin,$checkout,$guests)
	 {
	 			# code...



				$url = 'https://api.airbnb.com/v2/pricing_quotes';
				// $key = 'd306zoyjsyarp7ifhu67rjxn52tv0t20';
				$cid = $this->yup_options['yupclientid'];
				$uid = $this->yup_options['yupuid'];


				if( empty( $cid) || empty($uid)){
						$uid = '57297136';
						$cid = '3092nxybyb0otqw18e8nh5nty';
				}

				$apiData = array(
							'_intents' => 'p3_book_it',
							'_format' => 'for_detailed_booking_info_on_web_p3_with_message_data',
							'locale' => 'en',
							'currency' => 'VND',
							'show_smart_promotion' => '0',
						  'launchInfantsV2' => true,
							'client_id' => $cid,
							'listing_id' => $listing,
							'guests' => $guests,
							'check_in' => $checkin,
							'check_out' =>$checkout
				);

				$params = '';

				foreach($apiData as $key=>$value)
										$params .= $key.'='.$value.'&';

				$params = trim($params, '&');


				//  Initiate curl
				$ch = curl_init();
				// Disable SSL verification
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				// Will return the response, if false it print the response
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Set the url
				 curl_setopt($ch, CURLOPT_URL, $url.'?'.$params );
				// Execute
				$result=curl_exec($ch);

				//Get the resulting HTTP status code from the cURL handle.
				$http_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

				// Closing
				curl_close($ch);

				$ret  = json_decode($result, true);

				$available = json_encode($ret['pricing_quotes'][0]['available']);
				return $available;

	 }



	 	/**
	 	 * Get list of Listing House
	 	 *
	 	 * @since    1.0.0
	 	 */

	 	 public function yupsearch()
	 	 {
	 	 			# code...
					//Get list of house ID and checkin, checkout + guest.


					$guests = isset($_POST['guests']) ? $_POST['guests'] : '1';
					$checkin = isset($_POST['checkin']) ? $_POST['checkin'] : '';
					$checkout = isset($_POST['checkout']) ? $_POST['checkout'] : '';
					$listings = (isset($_POST['listings']) && is_array($_POST['listings'])) ? implode(", ", $_POST['listings']) : null;



					if ( empty($checkin) || empty($checkout) || $listings === null )   {
							return ;
					}


						$url = 'https://api.airbnb.com/v2/pricing_quotes';
						// $key = 'd306zoyjsyarp7ifhu67rjxn52tv0t20';
						$cid = $this->yup_options['yupclientid'];
						$uid = $this->yup_options['yupuid'];


						if( empty( $cid) || empty($uid)){
								$uid = '57297136';
								$cid = '3092nxybyb0otqw18e8nh5nty';
						}

						$availableList = array();
						$listingArr =  $_POST['listings'];

						foreach ($listingArr as $listing) {


									$available = $this->CheckHouseAvailable($listing,$checkin,$checkout,$guests);

									if( $available ){
												$availableList[] = (object) array('id' => $listing, 'available' => $available, 'checkout'=> $checkout , 'checkin' => $checkin );
									}
						}

					//var_dump($availableList);

					echo json_encode($availableList);

					wp_die();
					die();

	 	 }



}
