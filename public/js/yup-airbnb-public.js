

// Setup Checkin Checkout Date

// make Map stiky when scroll
// function stickyMap(){
// 			var s = $("#house-map");
// 			var pos = s.position();
// 			$(window).scroll(function() {
// 					var windowpos = $(window).scrollTop();
//
// 					if (windowpos > pos.top + 20 ) {
// 								$('body').addClass("mapsticky");
// 					} else {
// 								$('body').removeClass("mapsticky");
// 					}
// 					google.maps.event.trigger(map, 'resize');
// 			});
// }

// Map and House handlers


// init data









// Search handle



var listingHandle = (function($) {

    'use strict';

		var searchForm  		= $('.search-options'),
				listingSection  = $('#listings'),
				infoSection 		= $('.list-info'),
				mapSection  		= $('#house-map'),
        btnReturn       = $('#btnReturn'),
				btnSearch   		= searchForm.find('.btn'),
				listActive  		= [],
				listAvailable 	= [],
				yupListAvailable = [],
				activeCity  		= $('#activeCity'),
				cityActive  		= 0;


		var yupLocations = [];
		var map;
		var marker;
		var markers = [];


		function checkIncheckOutDatePicker($obj1, $obj2){
				var nowTemp = new Date();
				var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

				var checkin = $($obj1).datepicker({
							onRender: function(date) {
										return date.valueOf() < now.valueOf() ? 'disabled' : '';
							}
				}).on('changeDate', function(ev) {
							if (ev.date.valueOf() > checkout.date.valueOf()) {
										var newDate = new Date(ev.date)
												newDate.setDate(newDate.getDate() + 1);

												checkout.setValue(newDate);
							}
							checkin.hide();
							$($obj2)[0].focus();

				}).data('datepicker');
				var checkout = $($obj2).datepicker({
						onRender: function(date) {
										return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
						}
				}).on('changeDate', function(ev) {
							checkout.hide();
				}).data('datepicker');
		}


		function showRating(){

				$('.card-house').each(function() {

						var self = $(this);
						var rateHolder = self.find('.card-rate .rate');
						var rate = self.find('.card-rate .rate').data('rate');

						switch (rate) {
							case 1:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i>');
								break;
							case 1.5:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half" aria-hidden="true"></i>');
								break;
							case 2:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>');
								break;

							case 2.5:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half" aria-hidden="true"></i>');
								break;
							case 3:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>');
								break;

							case 3.5:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half" aria-hidden="true"></i>');
								break;
							case 4:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>');
								break;

							case 4.5:
								rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half" aria-hidden="true"></i>');
								break;
							case 5:
									rateHolder.html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>');
									break;
							default:
										rateHolder.html('<i class="fa fa-star-o" aria-hidden="true"></i>');
										break;

						}

				});
		}

    function stickyMap(){
    			var s = $("#house-map");
    			var pos = s.position();
    			$(window).scroll(function() {
    					var windowpos = $(window).scrollTop();

    					if (windowpos > pos.top + 220 ) {
    								$('body').addClass("mapsticky");
    					} else {
    								$('body').removeClass("mapsticky");
    					}
    					google.maps.event.trigger(map, 'resize');
    			});
    }

		// hover on House and make pin on map

		function houseitemHoverHandle($item){

					$($item).hover(function(){
								var self = $(this);
								var houseid = self.data('id');

								for (var i = 0; i < yupLocations.length; i++) {
											var yuploc = yupLocations[i];
											if(yuploc[5]  === houseid){
													map.setCenter(markers[i].getPosition());
													google.maps.event.trigger(markers[i], 'click');
											}
								}
					});
		}

		// MAP
		function initDataMap(ListArr){
				//yupInitListing
				yupLocations.length = 0;

				$.each( ListArr , function( i, l ){
							var item = [];
							item.push(l.city);
							item.push(l.lat);
							item.push(l.lng);
							item.push(l.name);
							item.push(i);
							item.push(l.id);
							yupLocations.push(item);
				});

		}

		function initMap() {
		  		map = new google.maps.Map(document.getElementById('house-map'), {
		    				zoom: 14,
		    				center: {lat: 10.772168, lng: 106.698141},
								styles:[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#9ae2ff"},{"visibility":"on"}]}]
		  		        }

            );


		  		setMarkers(map);
					google.maps.event.trigger(map, 'resize');



		}


		function setMarkers(map) {
				  // Adds markers to the map.
				  // Shapes define the clickable region of the icon. The type defines an HTML
				  // <area> element 'poly' which traces out a polygon as a series of X,Y points.
				  // The final coordinate closes the poly by connecting to the first coordinate.

					var infowindow = null;
		    	var infoWindows = Array();
					var bounds = new google.maps.LatLngBounds();
					var latlng;
					markers.length = 0; //empty array;

					for (var i = 0; i < yupLocations.length; i++) {

						    var yuploc = yupLocations[i];
										marker = new google.maps.Marker({
										      position: {lat: yuploc[1], lng: yuploc[2]},
										      map: map,
													animation : google.maps.Animation.DROP,
										      title: yuploc[3],
										      zIndex: yuploc[4],
													html: yuploc[3],
													id: yuploc[4]
						    			});

								markers[i] = marker;

								var content = "<h3>" + yuploc[3] + "<h3>";
								infowindow = new google.maps.InfoWindow({
		 	                content: "loading..."
		 	          });

		            google.maps.event.addListener(marker, "click", function () {
		                infowindow.setContent(this.html);
		                infowindow.open(map, this);
		            });
				  }
		}






		//
		function resetListActivebyCity(){
					activeCity.find('li').removeClass('active');
					cityActive  = 0;
		}

		function setListActive(){
					$.each(yupInitListing, function (index, value) {
							listActive.push(value.id);
					});
		}

		function updateListActive(active){

		}


		function setlistAvailable(arr){

						listAvailable.length = 0;
						//if(arr)
						$.each(arr, function( index, value ) {

										if(value.available == "true" ){

													listAvailable.push({
                              "id": value.id ,
                              "checkin" : value.checkin,
                              "checkout" :value.checkout
                            });
										}
						});

		}

		function displayListings(){
						var source = $("#houseTemplate").html();
						var template = Handlebars.compile(source);
						var html = template(yupInitListing);
						$('#listings').html(html);
		}

		function displayAvailableList(){

						yupListAvailable = [];

						if(listAvailable.length <= 0) {

								infoSection.html('<p class=""> We cant find a house for you.</p> <a href="#" id="btnReturn" onClick="window.location.reload()" class="btnReturn btn btn-default btn-fill" title="Return List">Return List</a>');


						}else{


								infoSection.html('<p class="">We found '+ listAvailable.length + ' house for you.</p>');

								//handle list
								$.each(listAvailable, function( index, value ) {
											var hid = value.id;
                      var hcheckin = value.checkin;
                      var hcheckout = value.checkout;

											$.each(yupInitListing, function(idx, val){
													var houselookupID = val.id;

													if( houselookupID.toString() === hid) {

                                yupInitListing[idx].checkin = hcheckin;
                                yupInitListing[idx].checkout = hcheckout;
                                // add to list
																yupListAvailable.push(yupInitListing[idx]);
													}

											});

                      //update Marker List
                      initDataMap(yupListAvailable);
                      //update Map
                      initMap();

								});

								//display list

								var source = $("#houseTemplate").html();
								var template = Handlebars.compile(source);
								var html = template(yupListAvailable);
								$('#listings').html(html);

								showRating();

                houseitemHoverHandle('.card-house');
						}




		}


		function searchList(){
					var checkin = searchForm.find("#checkinDate").val();
					var checkout = searchForm.find("#checkoutDate").val();
					var guests = searchForm.find("#guest").val();
					//console.log("checkin: " + checkin  + " - check_out: " + checkout + " - guests: "  + guests );

					if(checkin ==='' || checkout ===''){ return;}

					var formData = {
									 "checkin" 	: checkin,
									 "checkout" : checkout,
									 'guests'		: guests,
									 'listings' : listActive,
									 'action'		: 'yupsearch'
						 };
						 $.ajaxSetup({

			            beforeSend: function () {
                      listingSection.html('');
											listingSection.append('<div class="loading"><i class="fa fa-spinner fa-2x fa-spin" aria-hidden="true"></i> Checking available ...</div>');

			            },
			            complete: function () {
                    	listingSection.html('');
			              	listingSection.find(".loading").fadeOut().remove();
											displayAvailableList();
			            }
			        });


						 $.ajax({
											type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
											url         : ret_search_listing_ajax.ajax_url,// the url where we want to POST
											data        : formData, // our data object
											dataType    : 'json', // what type of data do we expect back from the server
											encode      : true,
											crossDomain : true,
											isLocal		 : true

									}).success(function(ret) {
												setlistAvailable(ret);
									});

		}




		function initialFirst(){
					infoSection.html('');

					setListActive();
					resetListActivebyCity();
					checkIncheckOutDatePicker('#checkinDate', '#checkoutDate');

          displayListings();
					initDataMap(yupInitListing);
          initMap();
          houseitemHoverHandle('.card-house');
          showRating();
          stickyMap();
		}

    function init(){

				initialFirst(); //init reset data;

				// init a map with first data return



				// activeCity.find('li a').on('click', function(e){
				// 		e.preventDefault();
				// 		var self = $(this);
				// 		activeCity.find('li').removeClass('active');
				// 		self.parent().addClass('active');
				// 		cityActive  = self.parent().data('active');
				// 		setListActive(cityActive);
				// });



      	btnSearch.on('click', function(e){
      			e.preventDefault();
						searchList();
				});

    }

    return {
				init : init,
				searchList:searchList

		 };


})(jQuery);




(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 */
	 $( window ).load(function() {

		 		if($('body').hasClass('listing')){
              listingHandle.init();
        }


	 });

	 /* ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );
