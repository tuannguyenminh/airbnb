<?php

/**
 * Fired during plugin activation
 *
 * @link       https://linkedin.com/in/minhtuan2086
 * @since      1.0.0
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/includes
 * @author     TUAN NGUYEN minh <minhtuan2086@gmail.com>
 */
class Yup_Airbnb_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
