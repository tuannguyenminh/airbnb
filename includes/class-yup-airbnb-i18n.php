<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://linkedin.com/in/minhtuan2086
 * @since      1.0.0
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/includes
 * @author     TUAN NGUYEN minh <minhtuan2086@gmail.com>
 */
class Yup_Airbnb_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'yup-airbnb',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
