<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://linkedin.com/in/minhtuan2086
 * @since      1.0.0
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/includes
 * @author     TUAN NGUYEN minh <minhtuan2086@gmail.com>
 */
class Yup_Airbnb_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
