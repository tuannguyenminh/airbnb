<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://linkedin.com/in/minhtuan2086
 * @since      1.0.0
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/admin
 * @author     TUAN NGUYEN minh <minhtuan2086@gmail.com>
 */
class Yup_Airbnb_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->yup_options = get_option($this->plugin_name);
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Yup_Airbnb_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Yup_Airbnb_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name.'-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, null );
		//wp_enqueue_style( $this->plugin_name.'-dashboard', plugin_dir_url( __FILE__ ) . 'css/paper-dashboard.css', array(), $this->version,'all');
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/yup-airbnb-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Yup_Airbnb_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Yup_Airbnb_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

			//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/yup-airbnb-admin.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Register the MENU for the admin area.
	 *
	 * @since    1.0.0
	 */

	public function add_plugin_admin_menu() {
		    add_options_page( 'Airbnb Settings', 'Airbnb Settings', 'manage_options', $this->plugin_name, array($this, 'display_airbnb_setup_page'));
	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */

	public function display_airbnb_setup_page() {
			include_once( 'partials/yup-airbnb-settings-display.php' );
	}

	/**
	 * Update and Save the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */

	public function options_update() {
 		register_setting($this->plugin_name, $this->plugin_name, array($this, 'validate'));
 	}

	/**
	 * Validate the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */

	public function validate($input) {

    $valid = array();
    //Cleanup
    	$valid['yupclientid'] = esc_attr($input['yupclientid']);
     	$valid['yuptoken'] = esc_attr($input['yuptoken']);
     	$valid['yupuid'] = esc_attr($input['yupuid']);
     	$valid['yupguests'] = esc_attr($input['yupguests']);

    return $valid;
  }


	public function yup_page_template( $page_template ){
	    if ( is_page( 'listing' ) ) {
	        $page_template = dirname( __FILE__ ) . '/templates/page-listing.php';
	    }
	    return $page_template;
	}





}// end class
