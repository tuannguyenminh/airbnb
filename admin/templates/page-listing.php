<?php
/**
 * Template Name: Listing Template
 */
      $options = get_option('yup-airbnb', array());
      $yup_guests  = $options['yupguests'];

      $page_cover_image = get_field( "page_cover_image" );
      $page_title = get_field( "page_title" );
      $page_subtitle = get_field( "subtitle" );

     if( $page_cover_image ) {

         $pagecover = '<section class="page-cover jumbotron" style="background-image:url('. $page_cover_image .'); ">';
         $pagecover .= '<div class="container"><div class="col-md-8 col-md-offset-2">';
         $pagecover .= '<h1>'.$page_title.'</h1><p>'.$page_subtitle.'</p></div></div>';
         $pagecover .= '</section>';

         echo $pagecover;
     }
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<div class="container-fluid">
<div class="row">
      <div class="col-xs-12">

        <section class="section location collapse">
                  <div class="row">
                      <div class="col-sm-12">
                          <ul id="activeCity" class="list-inline" >
                              <li><a href="#" title="Ha noi" data-active="1">Ha noi</a></li>
                              <li><a href="#" title="Ho chi minh" data-active="2">Ho chi minh</a></li>
                          </ul>

                      </div>
                  </div>

        </section>

        <section class="section search-options">
            <div class="container">
            <div class="row">
              <div class="col-xs-12 col-md-10 col-md-offset-1">
                    <form class="form-inline">

                            <div class="form-group">
                              <input type="text" class="form-control" id="checkinDate" placeholder="checkin" data-date-format="yyyy-mm-dd">
                            </div>

                            <div class="form-group">
                              <input type="text" class="form-control" id="checkoutDate" placeholder="checkout" data-date-format="yyyy-mm-dd">
                            </div>

                            <div class="form-group">

                                    <select id="guest" class="selectpicker">
                                      <?php
                                          for ($i=1; $i <= $yup_guests; $i++) {
                                              if($i == 1) { ?>
                                          <option value="<?php echo $i; ?>"><?php echo $i; ?> guest</option>
                                            <?php }else{ ?>
                                                  <option value="<?php echo $i; ?>"><?php echo $i; ?> guests</option>
                                            <?php }
                                          }
                                       ?>
                                    </select>

                            </div>


                            <button type="button" class="btn btn-default btn-fill"  data-nonce="<?php echo wp_create_nonce('searchListing');?>"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                    </form>
              </div>
            </div>
            </div>
        </section>

        <section class=" section section-listing">
          <div class="row">
              <!--- house listing -->

              <div class="col-sm-12 col-md-7 col-xs-12">
                  <div class="list-info">

                  </div>

                  <div id="listings" class="listing-house">

                        <!--listing will be here -->


                        <!--//listing will be here -->
                        <div class="loading"><i class="fa fa-spinner fa-2x fa-spin" aria-hidden="true"></i> Checking available ...</div>
                  </div>

              </div>
              <!--- // house listing -->

              <!--- map house listing -->

              <div class="col-md-5 hidden-sm-down">
                  <div style="">
                        <div id="house-map" style="height:700px">

                        </div>
                  </div>

              </div>
              <!--- //map house listing -->

          </div>

        </section>

</div> <!-- end col -->
</div> <!-- end row -->
</div> <!-- end container -->


<script id="houseTemplate" type="text/x-handlebars-template">
    <div>
       {{#each this}}
        <div id="house-{{id}}" class="card card-horizontal card-house" data-market="{{market}}" data-id="{{id}}" data-lng="{{lng}}" data-lat="{{lat}}" data-city="{{city}}">
                     <div class="row">
                         <div class="col-md-5">
                             <div class="image" style="background-image: url({{picture_url}}); background-position: center center; background-size: cover;">
                                 <img src="{{picture_url}}" alt="{{name}}" style="display: none;">
                                 <!-- <div class="filter hidden">

                                 </div> -->
                                 <div class="price">from<span class="value">{{price}}</span><span class="currency">{{native_currency}}</span></div>
                             </div>
                         </div>
                         <div class="col-md-7">
                              <div class="content">

                                          <a class="card-link" target="_blank" href="https://www.airbnb.com/rooms/{{id}}">
                                              <h4 class="title">{{name}}</h4>
                                          </a>



                                 <p class="category  card-rate">
                                       <span class="rate" data-rate="{{star_rating}}">

                                       </span>
                                       <span class="reviews">{{reviews_count}} reviews on Airbnb</span>
                                 </p>

                                 <div class="infos">
                                     <div class="row">

                                       <div class="info col-xs-6 col-md-6">
                                           <label>Cleanliness</label>
                                           <div class="progress">
                                               <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                 <span class="sr-only">60% Complete</span>
                                               </div>
                                            </div>
                                       </div>

                                       <div class="info col-xs-6 col-md-6">
                                           <label>Comfort</label>
                                           <div class="progress">
                                               <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                 <span class="sr-only">60% Complete</span>
                                               </div>
                                            </div>
                                       </div>

                                       <div class="clearfix"></div>

                                       <div class="info col-xs-6 col-md-6">
                                           <label>Location</label>
                                           <div class="progress">
                                               <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                 <span class="sr-only">60% Complete</span>
                                               </div>
                                            </div>
                                       </div>
                                       <div class="info col-xs-6 col-md-6">
                                           <label>Facilities</label>
                                           <div class="progress">
                                               <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                 <span class="sr-only">60% Complete</span>
                                               </div>
                                            </div>
                                       </div>
                                         <div class="clearfix"></div>


                                     </div>

                                     <div class="statistics row">
                                        <div class="col-xs-6 col-md-3 text-center static">
                                              <i class="icon icon-entire-place icon-size-2" aria-hidden="true"></i>
                                              <span>{{room_type}}</span>
                                        </div>
                                        <div class="col-xs-6 col-md-3 text-center static">
                                              <i class="icon icon-group icon-size-2" aria-hidden="true" ></i>
                                                <span>{{person_capacity}} Guests</span>
                                        </div>
                                        <div class="col-xs-6 col-md-3 text-center static">
                                            <i class="icon icon-rooms icon-size-2" aria-hidden="true"></i>
                                              <span>{{bedrooms}} Bedrooms</span>
                                        </div>
                                        <div class="col-xs-6 col-md-3 text-center static">
                                            <i class="icon icon-double-bed icon-size-2" aria-hidden="true"></i>
                                              <span>{{beds}} Bed</span>
                                        </div>

                                     </div>
                                 </div>

                                  <div class="footer">

                                    {{#if this.checkin}}
                                              <a href="https://www.airbnb.com/rooms/{{id}}?checkin={{checkin}}&checkout={{checkout}}" title="Book {{name}} now" class="btn btn-default btn-fill" target="_blank">Book now</a>
                                          {{else}}
                                            <a href="https://www.airbnb.com/rooms/{{id}}" title="Book {{name}} now" class="btn btn-default btn-fill" target="_blank">Book now</a>
                                    {{/if}}


                                 </div>



                             </div>
                         </div>
                     </div>
                 </div>
        {{/each}}
    </div>

</script>
