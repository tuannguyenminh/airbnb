<?php

/**
 * Provide a admin settings view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://linkedin.com/in/minhtuan2086
 * @since      1.0.0
 *
 * @package    Yup_Airbnb
 * @subpackage Yup_Airbnb/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap ">
      <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
      <form method="post" name="yup_airbnb_options" action="options.php">


        <?php

          //Grab all options
          $options = get_option($this->plugin_name);

          $yupclientid = $options['yupclientid'];
          $yuptoken = $options['yuptoken'];
          $yupuid = $options['yupuid'];
          $yupguests = $options['yupguests'];


          settings_fields($this->plugin_name);
          do_settings_sections($this->plugin_name);


        ?>



        <div class="card">

                      <div class="header">
                          <h4 class="title">Client ID and User ID</h4>
                          <p class="category">Log into Airbnb.com, open up the web developer console, go to the network tab, filter by type json, and look at the url and find "client_id".</p>
                      </div>

                      <div class="content">
                          <div class="row">
                                <div class="form-group">
                                    <legend class="screen-reader-text"><span>Client ID</span></legend>
                                    <input id="<?php echo $this->plugin_name; ?>-yupclientid" type="text" name="<?php echo $this->plugin_name; ?>[yupclientid]" class="form-control border-input regular-text" placeholder="3092nxybyb0otqw18e8nh5nty" value="<?php if(!empty($yupclientid)) echo $yupclientid; ?>">
                                </div>
                          </div>
                          <div class="row">
                                <div class="form-group">
                                    <label for="<?php echo $this->plugin_name; ?>-yupuid">User ID</label>
                                    <legend class="screen-reader-text"><span>User ID</span></legend>
                                    <input id="<?php echo $this->plugin_name; ?>-yupuid" type="text" name="<?php echo $this->plugin_name; ?>[yupuid]" class="form-control border-input regular-text" placeholder="" value="<?php if(!empty($yupuid)) echo $yupuid; ?>">
                                </div>
                          </div>
                      </div>


                      <div class="header">
                          <h4 class="title">Settings</h4>
                          <p class="category"></p>
                      </div>

                      <div class="content">


                          <div class="row">
                              <div class="form-group">
                                  <label for="<?php echo $this->plugin_name; ?>-yuptoken">Token key </label>
                                  <legend class="screen-reader-text"><span>Token Key</span></legend>
                                  <input id="<?php echo $this->plugin_name; ?>-yuptoken" type="text" name="<?php echo $this->plugin_name; ?>[yuptoken]" class="form-control border-input regular-text" placeholder="" value="<?php if(!empty($yuptoken)) echo $yuptoken; ?>">
                              </div>
                          </div>

                          <div class="row">
                              <div class="form-group">
                                  <label for="<?php echo $this->plugin_name; ?>-guests">Number of guests</label>
                                  <legend class="screen-reader-text"><span>Number of guests</span></legend>
                                  <input id="<?php echo $this->plugin_name; ?>-yupguests" type="text" name="<?php echo $this->plugin_name; ?>[yupguests]" class="form-control border-input regular-text" placeholder="" value="<?php if(!empty($yupguests)) echo $yupguests; ?>">
                              </div>
                          </div>


                      </div>




          </div>





              <?php submit_button('Save all changes', 'primary btn-info','submit', TRUE); ?>


      </form>

</div>
